﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetLocalSK.Models
{
    public class Point
    {
        public int Id { get; set; }

        public int CoordinatesId { get; set; }

        public string Name { get; set; }

        public virtual Coordinates Coordinates { get; set; }
    }
}
