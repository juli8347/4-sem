﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetLocalSK.Models
{
    public class Coordinates
    {
        public int Id { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
