﻿using GetLocalSK.Models;
using GetLocalSK.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GetLocalSK.ViewModels
{
    public class MapViewModel : ViewModelBase
    {
        private readonly DataRetrieval _data;

        private ObservableCollection<Coordinates> _coordinates;
        public ObservableCollection<Coordinates> Coordinates
        {
            get { return _coordinates; }
            set { _coordinates = value; OnPropertyChanged(nameof(Coordinates)); }
        }

        public MapViewModel(DataRetrieval data)
        {
            _data = data;
            getData();
        }

        private async void getData()
        {
            Coordinates = new ObservableCollection<Coordinates>(await _data.GetCoordinatesAsync());
        }
    }
}
