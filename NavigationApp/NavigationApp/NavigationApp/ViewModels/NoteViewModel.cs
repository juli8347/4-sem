﻿using NavigationApp.Models;
using NavigationApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace NavigationApp.ViewModels
{
    public class NoteViewModel
    {
        private ObservableCollection<Note> _notes;

        public ObservableCollection<Note> Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        public INavigation Navigation { get; set; }

        public NoteViewModel(INavigation navigation)
        {
            _notes = new ObservableCollection<Note>
            {
                new Note
                {
                    Title = "Hierarchical navigation",
                    Text = "The NavigationPage class provides a hierarchical navigation experience " +
                    "where the user is able to navigate through pages, forwards and backwards, as " +
                    "desired. The class implements navigation as a last-in, first-out (LIFO) stack " +
                    "of Page objects."
                },
                new Note
                {
                    Title = "Shopping list",
                    Text = "Fruit and veggies:\n- Lettuce\n- Tomatoes\n- Apples\n\n" +
                    "Animal products:\n- Tempura prawns\n- Salmon\n- Eggs\n- Honey\n\n" +
                    "Colonial goods:\n- Bread\n- Spaghetti\n- Quinoa\n\n" +
                    "Snacks:\n- Cookies\n- Chocolate bar"
                },
                new Note
                {
                    Title = "To-do list 1/3",
                    Text = "\n- Go grocery shopping.\n- Food prep for the week.\n- Yoga practice.\n" +
                    "- Pick up siblings.\n- Build an app.\n- Do laundry, white clothes."
                },
                new Note
                {
                    Title = "Remember",
                    Text = "Pick up Philip from football practice."
                },
                new Note
                {
                    Title = "Vacation packing",
                    Text = "Don't forget your face masks and hand sanitizer. And pack an extra pair " +
                    "of shoes in case of heavy rain."
                }
            };

            this.Navigation = navigation;

            TapCmd = new Command<Label>(ViewNote);
            SwipeCmd = new Command<Label>(DeleteNote);
        }

        private async void ViewNote(Label label)
        {
            Note noteToView = new Note();

            foreach(Note note in Notes)
            {
                if (note.Title == label.Text)
                {
                    noteToView = note;
                    break;
                }
            }
            await Navigation.PushAsync(new NoteDetailsView(noteToView));
        }

        private void DeleteNote(Label label)
        {
            foreach (Note note in Notes)
            {
                if(note.Title == label.Text)
                {
                    Notes.Remove(note);
                    break;
                }
            }
        }

        public ICommand TapCmd { get; }
        public ICommand SwipeCmd { get; }
    }
}
