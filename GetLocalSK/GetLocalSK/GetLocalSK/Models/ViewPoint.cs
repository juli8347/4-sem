﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetLocalSK.Models
{
    public class ViewPoint : Point
    {
        public int PlaceId { get; set; }

        public virtual Place Place { get; set; }
    }
}
