﻿using GetLocalSK.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GetLocalSK.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapView : ContentPage
    {
        MapViewModel vm;
        public MapView()
        {
            InitializeComponent();

            vm = BindingContext as MapViewModel;
        }
    }
}