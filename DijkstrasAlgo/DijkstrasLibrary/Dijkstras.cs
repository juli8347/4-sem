﻿using System;

namespace DijkstrasLibrary
{
    public static class Dijkstras
    {
        // The graph argument is an undirected graph w/ or w/o edge weights.
        public static int[] DijkstraMain(int[,] graph, int source)
        {
            int verticesCount = graph.GetLength(0);

            // Array to be returned - represents the shortest path tree set.
            // Will contain shortest path from source to all vertices in graph.
            int[] sptSet = new int[verticesCount]; 

            // isVisited[i] holds true, if shortest distance from source has been found.
            bool[] isVisited = new bool[verticesCount];

            // Init above arrays.
            // All shortest paths is "infinite" at this point besides the source vertex.
            // No vertices have been visited yet, hence only false values.
            for (int i = 0; i < verticesCount; i++)
            {
                if (i == source)
                    sptSet[i] = 0;
                else
                    sptSet[i] = int.MaxValue; 


                isVisited[i] = false;
            }

            // Main loop.
            // Goes through all vertices until none remains unvisited.
            // Starts from the vertex with the shortest path to the longest.
            for (int i = 0; i < verticesCount; i++)
            {
                int currVertex = getNextVertex(sptSet, isVisited, source);

                isVisited[currVertex] = true;

                for (int j = 0; j < verticesCount; j++)
                {
                    // If j is adjacent to currVertex, and j haven't been visited.
                    if (graph[currVertex, j] != 0 && !isVisited[j])
                    {
                        // Total distance from source to j through currVertex.
                        int totalDist = sptSet[currVertex] + graph[currVertex, j];

                        // If totalDistance is smaller than the current value of sptSet[j],
                        // the new value of sptSet[j] equals totalDist.
                        if (totalDist < sptSet[j])
                            sptSet[j] = totalDist;
                    }
                }
            }
            return sptSet;
        }

        // Finds unvisited vertex with shortest path from source.
        private static int getNextVertex(int[] sptSet, bool[] isVisited, int source)
        {
            int currShortest = int.MaxValue; 
            int vertex = -1;

            for (int i = 0; i < sptSet.Length; i++)
            {
                if (!isVisited[i] && sptSet[i] <= currShortest)
                {
                    if (i == source)
                        return i;

                    currShortest = sptSet[i];
                    vertex = i;
                }
            }

            return vertex;
        }




        //public static void CreateSptSet(int[,] graph)
        //{

        //}

        //private static void findShortestPath(int[,] graph, int source)
        //{
        //    // SETUP
        //    int verticesCount = graph.GetLength(0);
        //    int?[] unvisitedVertices = new int?[verticesCount];
        //    int?[,] shortestDists = new int?[verticesCount, 2]; // [dist, previous vertex]

        //    for (int i = 0; i < verticesCount; i++)
        //    {
        //        unvisitedVertices[i] = i;

        //        if (i == source)
        //        {
        //            shortestDists[source, 0] = 0;
        //            shortestDists[source, 1] = null;
        //        }
        //        else
        //        {
        //            shortestDists[i, 0] = Int32.MaxValue;
        //        }
        //    }

        //    //
        //    int prevVertex;
        //    int currVertex = source;
        //    int vertexToCompare;
            
        //    while(unvisitedVertices.Length != 0)
        //    {
        //        int[] adjacentVertices = findAdjacentVertices(verticesCount, graph, currVertex);
        //        unvisitedVertices = new 
        //        //int[] currShortest = new int[] { Int32.MaxValue, -1 }; // { dist, vertex }

        //        prevVertex = currVertex;

        //        for (int i = 0; i < adjacentVertices.Length; i++)
        //        {
        //            vertexToCompare = adjacentVertices[i];
        //            int dist = graph[currVertex, vertexToCompare];
        //            int? currShortest = shortestDists[vertexToCompare, 0];

        //            if (dist < currShortest)
        //            {
        //                shortestDists[vertexToCompare, 0] = dist;
        //                shortestDists[vertexToCompare, 1] = currVertex;
        //            }
        //        }
        //    }


        //    for (int i = 0; i < verticesCount; i++)
        //    {
        //        int dist = graph[currVertex, i];
        //        int? currMinDist = shortestDists[i, 0];
        //    }
        //}

        //private static int[] findAdjacentVertices(int verticesCount, int[,] graph, int currVertex)
        //{
        //    int[] adjacentVertices = new int[verticesCount];
        //    int adjacencyCounter = 0;

        //    for (int i = 0; i < verticesCount; i++)
        //    {
        //        if (graph[currVertex, i] != 0)
        //        {
        //            adjacentVertices[adjacencyCounter] = currVertex;
        //            adjacencyCounter++;
        //        }
        //    }

        //    return adjacentVertices;
        //}
    }
}
