﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GetLocalSK.Models;
using Microsoft.EntityFrameworkCore;

namespace GetLocalSK.Services
{
    public class DataRetrieval
    {
        private readonly DatabaseContext _databaseContext;

        public DataRetrieval()
        {
            _databaseContext = new DatabaseContext();
        }

        public async Task<IEnumerable<Coordinates>> GetCoordinatesAsync()
        {
            try
            {
                var coordinates = await _databaseContext.Coordinates.ToListAsync();
                return coordinates;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<IEnumerable<Place>> GetPlacesAsync()
        {
            try
            {
                var place = await _databaseContext.Places.ToListAsync();
                return place;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<List<ViewPoint>> GetViewPointsAsync()
        {
            try
            {
                var viewPoints = await _databaseContext.ViewPoints.ToListAsync();
                return viewPoints;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<List<Point>> GetPointsAsync()
        {
            try
            {
                var points = await _databaseContext.Points.ToListAsync();
                return points;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<List<PointToPointMap>> GetPointToPointMapsAsync()
        {
            try
            {
                var pointToPointMaps = await _databaseContext.PointToPointMaps.ToListAsync();
                return pointToPointMaps;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<List<Paragraph>> GetParagraphsAsync()
        {
            try
            {
                var paragraphs = await _databaseContext.Paragraphs.ToListAsync();
                return paragraphs;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
