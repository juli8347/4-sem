﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace NavigationApp.Models
{
    public class Note : INotifyPropertyChanged
    {
        private string _title;
        private string _text;

        public string Title 
        {
            get { return _title; }
            set { _title = value; OnPropertyChanged(nameof(Title)); }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; OnPropertyChanged(nameof(Text)); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
