﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace GetLocalSK.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        //Allows all classes that implements this class
        //to use 'OnPropertyChanged' seamlessly

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
