﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetLocalSK.Models
{
    public class Paragraph
    {
        public int Id { get; set; }

        public int PlaceId { get; set; }

        public string Header { get; set; }

        public string Text { get; set; }

        public virtual Place Place { get; set; }
    }
}
