﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetLocalSK.Models
{
    public class PointToPointMap
    {
        public int Id { get; set; }

        public int PointAId { get; set; }

        public int PointBId { get; set; }

        public string Name { get; set; }

        public double? Distance { get; set; }
    }
}
