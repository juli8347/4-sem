﻿using NavigationApp.ViewModels;
using NavigationApp.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NavigationApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new MainPage();

            MainPage = new NavigationPage(new NoteView());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
