﻿using NavigationApp.Models;
using NavigationApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NavigationApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoteDetailsView : ContentPage
    {
        public NoteDetailsView(Note note)
        {
            InitializeComponent();

            BindingContext = new NoteDetailsViewModel(note);
        }
    }
}