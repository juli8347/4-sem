﻿using GetLocalSK.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Xamarin.Essentials;

namespace GetLocalSK.Services
{
    public class DatabaseContext : DbContext
    { 
        public DbSet<Coordinates> Coordinates { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<ViewPoint> ViewPoints { get; set; }
        public DbSet<Point> Points { get; set; }
        public DbSet<PointToPointMap> PointToPointMaps { get; set; }
        public DbSet<Paragraph> Paragraphs { get; set; }

        public DatabaseContext()
        {
            SQLitePCL.Batteries_V2.Init();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string dbPath = Path.Combine(FileSystem.AppDataDirectory, "places.db3");

            optionsBuilder.UseSqlite($"Filename={dbPath}").UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Coordinates>()
                .ToTable(nameof(Models.Coordinates))
                .HasData(
                new Coordinates { Id = 1, Latitude = 55.6931313m, Longitude = 8.8403475m }, //tgh
                new Coordinates { Id = 2, Latitude = 55.6939885m, Longitude = 8.8395643m }, //lss
                new Coordinates { Id = 3, Latitude = 55.6943770m, Longitude = 8.8390088m }, //b&b
                new Coordinates { Id = 4, Latitude = 55.6945430m, Longitude = 8.8377342m }, //et 
                new Coordinates { Id = 5, Latitude = 55.6949740m, Longitude = 8.8409816m }, //sk 
                new Coordinates { Id = 6, Latitude = 55.6931684m, Longitude = 8.8400887m }, //1 
                new Coordinates { Id = 7, Latitude = 55.6933544m, Longitude = 8.8401964m }, //2 
                new Coordinates { Id = 8, Latitude = 55.6936527m, Longitude = 8.8399776m }, //3
                new Coordinates { Id = 9, Latitude = 55.6940893m, Longitude = 8.8401170m }, //4
                new Coordinates { Id = 10, Latitude = 55.6942992m, Longitude = 8.8393812m }, //22
                new Coordinates { Id = 11, Latitude = 55.6942037m, Longitude = 8.8399246m }, //23
                new Coordinates { Id = 12, Latitude = 55.6941361m, Longitude = 8.8399052m }, //24
                new Coordinates { Id = 13, Latitude = 55.6940804m, Longitude = 8.8390389m }, //25
                new Coordinates { Id = 14, Latitude = 55.6938228m, Longitude = 8.8389648m }, //26
                new Coordinates { Id = 15, Latitude = 55.6943499m, Longitude = 8.8391571m }, //21
                new Coordinates { Id = 16, Latitude = 55.6946844m, Longitude = 8.8380265m }, //19
                new Coordinates { Id = 17, Latitude = 55.6949445m, Longitude = 8.8408017m }, //10
                new Coordinates { Id = 18, Latitude = 55.6950480m, Longitude = 8.8407876m }, //11
                new Coordinates { Id = 19, Latitude = 55.6939569m, Longitude = 8.8408566m }, //5 
                new Coordinates { Id = 20, Latitude = 55.6939917m, Longitude = 8.8409801m }, //6 
                new Coordinates { Id = 21, Latitude = 55.6944552m, Longitude = 8.8413330m }, //7 
                new Coordinates { Id = 22, Latitude = 55.6945964m, Longitude = 8.8408372m }, //8 
                new Coordinates { Id = 23, Latitude = 55.6946799m, Longitude = 8.8405037m }, //9 
                new Coordinates { Id = 24, Latitude = 55.6950797m, Longitude = 8.8406766m }, //12
                new Coordinates { Id = 25, Latitude = 55.6951901m, Longitude = 8.8407842m }, //13 
                new Coordinates { Id = 26, Latitude = 55.6954118m, Longitude = 8.8399761m }, //14 
                new Coordinates { Id = 27, Latitude = 55.6951533m, Longitude = 8.8397926m }, //15
                new Coordinates { Id = 28, Latitude = 55.6951871m, Longitude = 8.8396249m }, //16
                new Coordinates { Id = 29, Latitude = 55.6948480m, Longitude = 8.8395508m }, //17
                new Coordinates { Id = 30, Latitude = 55.6950896m, Longitude = 8.8382345m }, //18
                new Coordinates { Id = 31, Latitude = 55.6945551m, Longitude = 8.8393002m }, //20
                new Coordinates { Id = 32, Latitude = 55.6933640m, Longitude = 8.8389072m }, //27
                new Coordinates { Id = 33, Latitude = 55.6932645m, Longitude = 8.8392177m } //28
                );

            modelBuilder.Entity<Place>()
                .ToTable(nameof(Place))
                .HasData(
                new Place { Id = 1, Name = "Teglgaardshallen", HeaderImage = null, CoordinatesId = 1 },
                new Place { Id = 2, Name = "Lynghedeskolen Stenderup", HeaderImage = null, CoordinatesId = 2 },
                new Place { Id = 3, Name = "Smedens Have B&B", HeaderImage = null, CoordinatesId = 3 },
                new Place { Id = 4, Name = "Erlings Transport", HeaderImage = null, CoordinatesId = 4 },
                new Place { Id = 5, Name = "Stenderup Kirke", HeaderImage = null, CoordinatesId = 5 }
                );

            modelBuilder.Entity<ViewPoint>()
                .ToTable(nameof(ViewPoint))
                .HasData(
                new ViewPoint { Id = 6, Name = "1-Teglgaardshallen", PlaceId = 1, CoordinatesId = 6 }, 
                new ViewPoint { Id = 7, Name = "2-Teglgaardshallen", PlaceId = 1, CoordinatesId = 7 }, 
                new ViewPoint { Id = 8, Name = "3-LynghedeskolenStenderup", PlaceId = 2, CoordinatesId = 8 }, 
                new ViewPoint { Id = 9, Name = "4-LynghedeskolenStenderup", PlaceId = 2, CoordinatesId = 9 }, 
                new ViewPoint { Id = 10, Name = "22-LynghedeskolenStenderup", PlaceId = 2, CoordinatesId = 10 }, 
                new ViewPoint { Id = 11, Name = "23-LynghedeskolenStenderup", PlaceId = 2, CoordinatesId = 11 }, 
                new ViewPoint { Id = 12, Name = "24-LynghedeskolenStenderup", PlaceId = 2, CoordinatesId = 12 }, 
                new ViewPoint { Id = 13, Name = "25-LynghedeskolenStenderup", PlaceId = 2, CoordinatesId = 13 }, 
                new ViewPoint { Id = 14, Name = "26-LynghedeskolenStenderup", PlaceId = 2, CoordinatesId = 14 }, 
                new ViewPoint { Id = 15, Name = "21-SmedensHaveBB", PlaceId = 3, CoordinatesId = 15 }, 
                new ViewPoint { Id = 16, Name = "19-Erlings", PlaceId = 4, CoordinatesId = 16 }, 
                new ViewPoint { Id = 17, Name = "10-StenderupKirke", PlaceId = 5, CoordinatesId = 17 }, 
                new ViewPoint { Id = 18, Name = "11-StenderupKirke", PlaceId = 5, CoordinatesId = 18 } 
                );

            modelBuilder.Entity<Point>()
                .ToTable(nameof(Point))
                .HasData(
                new Point { Id = 19, Name = "5-Buen", CoordinatesId = 19 },
                new Point { Id = 20, Name = "6-Buen", CoordinatesId = 20 },
                new Point { Id = 21, Name = "7-Buen-Østergaardsvej", CoordinatesId = 21 },
                new Point { Id = 22, Name = "8-Østergaardsvej", CoordinatesId = 22 },
                new Point { Id = 23, Name = "9-Østergaardsvej-Kike", CoordinatesId = 23 },
                new Point { Id = 24, Name = "12-Kirke", CoordinatesId = 24 },
                new Point { Id = 25, Name = "13-Kirke", CoordinatesId = 25 },
                new Point { Id = 26, Name = "14-Storegade-Kirke", CoordinatesId = 26 },
                new Point { Id = 27, Name = "15-Storegade", CoordinatesId = 27 },
                new Point { Id = 28, Name = "16-Storegade", CoordinatesId = 28 },
                new Point { Id = 29, Name = "17-KrydsStenderup", CoordinatesId = 29 },
                new Point { Id = 30, Name = "18-Søndergaardsvej-Åtoftevej", CoordinatesId = 30 },
                new Point { Id = 31, Name = "20-Teglgaardsvej", CoordinatesId = 31 },
                new Point { Id = 32, Name = "27-Teglgaardsvej", CoordinatesId = 32 },
                new Point { Id = 33, Name = "28-Teglgaardsvej", CoordinatesId = 33 }
                );

            modelBuilder.Entity<PointToPointMap>()
                .ToTable(nameof(PointToPointMap))
                .HasKey(p => new { p.PointAId, p.PointBId });
            modelBuilder.Entity<PointToPointMap>()
                .HasData(
                new PointToPointMap { Id = 1, Name = "1-2", PointAId = 6, PointBId = 7, Distance = null },
                new PointToPointMap { Id = 2, Name = "2-3", PointAId = 7, PointBId = 8, Distance = null },
                new PointToPointMap { Id = 3, Name = "3-4", PointAId = 8, PointBId = 9, Distance = null },
                new PointToPointMap { Id = 4, Name = "4-5", PointAId = 9, PointBId = 19, Distance = null },
                new PointToPointMap { Id = 5, Name = "5-6", PointAId = 19, PointBId = 20, Distance = null },
                new PointToPointMap { Id = 6, Name = "6-7", PointAId = 20, PointBId = 21, Distance = null },
                new PointToPointMap { Id = 7, Name = "7-8", PointAId = 21, PointBId = 22, Distance = null },
                new PointToPointMap { Id = 8, Name = "8-9", PointAId = 22, PointBId = 23, Distance = null },
                new PointToPointMap { Id = 9, Name = "9-10", PointAId = 23, PointBId = 17, Distance = null },
                new PointToPointMap { Id = 10, Name = "10-11", PointAId = 17, PointBId = 18, Distance = null },
                new PointToPointMap { Id = 11, Name = "11-12", PointAId = 18, PointBId = 24, Distance = null },
                new PointToPointMap { Id = 12, Name = "12-13", PointAId = 24, PointBId = 25, Distance = null },
                new PointToPointMap { Id = 13, Name = "13-14", PointAId = 25, PointBId = 26, Distance = null },
                new PointToPointMap { Id = 14, Name = "14-15", PointAId = 26, PointBId = 27, Distance = null },
                new PointToPointMap { Id = 15, Name = "15-16", PointAId = 27, PointBId = 28, Distance = null },
                new PointToPointMap { Id = 16, Name = "15-17", PointAId = 27, PointBId = 29, Distance = null },
                new PointToPointMap { Id = 17, Name = "17-18", PointAId = 29, PointBId = 30, Distance = null },
                new PointToPointMap { Id = 18, Name = "18-19", PointAId = 30, PointBId = 16, Distance = null },
                new PointToPointMap { Id = 19, Name = "17-9", PointAId = 29, PointBId = 23, Distance = null },
                new PointToPointMap { Id = 10, Name = "17-20", PointAId = 29, PointBId = 31, Distance = null },
                new PointToPointMap { Id = 21, Name = "20-21", PointAId = 31, PointBId = 15, Distance = null },
                new PointToPointMap { Id = 22, Name = "21-22", PointAId = 15, PointBId = 10, Distance = null },
                new PointToPointMap { Id = 23, Name = "22-23", PointAId = 10, PointBId = 11, Distance = null },
                new PointToPointMap { Id = 24, Name = "23-24", PointAId = 11, PointBId = 12, Distance = null },
                new PointToPointMap { Id = 25, Name = "24-4", PointAId = 12, PointBId = 9, Distance = null },
                new PointToPointMap { Id = 26, Name = "21-25", PointAId = 15, PointBId = 13, Distance = null },
                new PointToPointMap { Id = 27, Name = "25-26", PointAId = 13, PointBId = 14, Distance = null },
                new PointToPointMap { Id = 28, Name = "26-27", PointAId = 14, PointBId = 32, Distance = null },
                new PointToPointMap { Id = 29, Name = "27-28", PointAId = 32, PointBId = 33, Distance = null },
                new PointToPointMap { Id = 30, Name = "28-1", PointAId = 33, PointBId = 6, Distance = null }
                );

            modelBuilder.Entity<Paragraph>()
                .ToTable(nameof(Paragraph))
                .HasData(
                new Paragraph { Id = 1, Header = "Om Teglgaardshallen", Text = "Teglgaardshallen er Stenderup-Krogagers idrætshal og samlingssted når det drejer sig om fester. Den består af en minihal samt Værestedet med det dertilhørende køkken.", PlaceId = 1 },
                new Paragraph { Id = 2, Header = "Fitnesscenter", Text = "I Teglgaardshallen finder du også et nyt og moderne fitnesscenter, som bliver flittigt brugt af byens borgere, samt af de folk, der kommer fra de nærliggende byer.", PlaceId = 1 },
                new Paragraph { Id = 3, Header = "Skolen", Text = "Skolen er et-sporet med elever i 0. – 6. klasse. Der arbejdes med aldersblandet undervisning, undervisning i hold og undervisning på årgangen.", PlaceId = 2 },
                new Paragraph { Id = 4, Header = "Børnehaven", Text = "Børnene er fordelt på to aldersblandede grupper Sanglærker og Bogfinker. ", PlaceId = 2 },
                new Paragraph { Id = 5, Header = "Opdateres...", Text = "Opdateres...", PlaceId = 3 },
                new Paragraph { Id = 6, Header = "Opdateres...", Text = "Opdateres...", PlaceId = 4 },
                new Paragraph { Id = 7, Header = "Opdateres...", Text = "Opdateres...", PlaceId = 5 }
                );
        }
    }
}
