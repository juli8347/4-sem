using DijkstrasLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // arrange
            int[,] graph = new int[,]
            {
                { 0, 1, 0, 1 },
                { 1, 0, 1, 1 },
                { 0, 1, 0, 1 },
                { 1, 1, 1, 0 }
            };

            int source = 1;

            // act
            int[] sptSet = Dijkstras.DijkstraMain(graph, source);

            // assert
            Assert.AreEqual(1, sptSet[3]);
        }

        [TestMethod]
        public void TestMethod2()
        {
            // arrange
            int[,] graph = new int[,]
            {
                { 0, 1, 0, 2 },
                { 1, 0, 7, 0 },
                { 0, 7, 0, 3 },
                { 2, 0, 3, 0 }
            };

            int source = 1;

            // act
            int[] sptSet = Dijkstras.DijkstraMain(graph, source);

            // assert
            Assert.AreEqual(6, sptSet[2]);
        }

        [TestMethod]
        public void TestMethod3()
        {
            // arrange
            int[,] graph = new int[,]
            {
                { 0, 4 },
                { 4, 0 }
            };

            int source = 1;

            // act
            int[] sptSet = Dijkstras.DijkstraMain(graph, source);

            // assert
            Assert.AreEqual(4, sptSet[0]);
        }

        [TestMethod]
        public void TestMethod4()
        {
            // arrange
            int[,] graph = new int[,]
            {
                { 0, 3, 0, 0, 0, 0, 0, 0, 0, 27 },
                { 3, 0, 2, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 2, 0, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 1, 0, 2, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 2, 0, 2, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 2, 0, 1, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 0, 3, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 3, 0, 2, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 2, 0, 4 },
                { 27, 0, 0, 0, 0, 0, 0, 0, 4, 0 }
            };

            int source = 0;

            // act
            int[] sptSet = Dijkstras.DijkstraMain(graph, source);

            // assert
            Assert.AreEqual(20, sptSet[9]);
        }
    }
}
