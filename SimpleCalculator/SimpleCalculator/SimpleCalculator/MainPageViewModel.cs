﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace SimpleCalculator
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private int result;
        public int Result
        {
            get => result;
            set
            {
                result = value;

                var args = new PropertyChangedEventArgs(nameof(Result));

                PropertyChanged?.Invoke(this, args);
            }
        }

        private string expressionString;
        public string ExpressionString
        {
            get => expressionString;
            set
            {
                expressionString = value;

                var args = new PropertyChangedEventArgs(nameof(ExpressionString));

                PropertyChanged?.Invoke(this, args);
            }
        }

        List<int?> operands = new List<int?>();
        List<string> operators = new List<string>();
        string operand = "";

        public MainPageViewModel()
        {
            Result = 0;
            ExpressionString = "";

            Btn0 = new Command(() =>
            {
                btn0Action();
            });

            Btn1 = new Command(() =>
            {
                btn1Action();
            });

            Btn2 = new Command(() =>
            {
                btn2Action();
            });

            Btn3 = new Command(() =>
            {
                btn3Action();
            });

            Btn4 = new Command(() =>
            {
                btn4Action();
            });

            Btn5 = new Command(() =>
            {
                btn5Action();
            });

            Btn6 = new Command(() =>
            {
                btn6Action();
            });

            Btn7 = new Command(() =>
            {
                btn7Action();
            });

            Btn8 = new Command(() =>
            {
                btn8Action();
            });

            Btn9 = new Command(() =>
            {
                btn9Action();
            });

            BtnAdd = new Command(() =>
            {
                btnAddAction();
            });

            BtnSubtract = new Command(() =>
            {
                btnSubtractAction();
            });

            BtnMultiply = new Command(() =>
            {
                btnMultiplyAction();
            });

            BtnDivide = new Command(() =>
            {
                btnDivideAction();
            });

            BtnEquals = new Command(() =>
            {
                btnEqualsAction();
            });

            BtnCE = new Command(() =>
            {
                btnCEAction();
            });
        }

        private void calculate()
        {
            for (int i = 0; i < operators.Count(); i++)
            {
                string _operator = operators[i];
                switch (_operator)
                {
                    case "×":
                        operands[i + 1] = operands[i] * operands[i + 1];
                        operands[i] = null;
                        operators[i] = null;
                        break;

                    case "÷":
                        operands[i + 1] = operands[i] / operands[i + 1];
                        operands[i] = null;
                        operators[i] = null;
                        break;

                    default:
                        break;
                }
            }

            updateLists();

            int tempResult = 0;
            for (int j = 0; j < operands.Count(); j++)
            {
                tempResult += operands[j] ?? default;
            }

            Result = tempResult;
        }

        #region Command actions
        private void btn0Action()
        {
            ExpressionString += "0";
            operand += "0";
        }

        private void btn1Action()
        {
            ExpressionString += "1";
            operand += "1";
        }

        private void btn2Action()
        {
            ExpressionString += "2";
            operand += "2";
        }

        private void btn3Action()
        {
            ExpressionString += "3";
            operand += "3";
        }

        private void btn4Action()
        {
            ExpressionString += "4";
            operand += "4";
        }

        private void btn5Action()
        {
            ExpressionString += "5";
            operand += "5";
        }

        private void btn6Action()
        {
            ExpressionString += "6";
            operand += "6";
        }

        private void btn7Action()
        {
            ExpressionString += "7";
            operand += "7";
        }

        private void btn8Action()
        {
            ExpressionString += "8";
            operand += "8";
        }

        private void btn9Action()
        {
            ExpressionString += "9";
            operand += "9";
        }

        private void btnAddAction()
        {
            string _operator = "+";
            ExpressionString += $" {_operator} ";

            addToLists(_operator);
        }

        private void btnSubtractAction()
        {
            string _operator = "-";
            ExpressionString += $" {_operator} ";

            if (!operands.Any() && operand == "")
            {
                operand = _operator;
            }
            else
            {
                operands.Add(Int32.Parse(operand));
                operand = _operator;
                operators.Add("+");
            }
        }

        private void btnMultiplyAction()
        {
            string _operator = "×";
            ExpressionString += $" {_operator} ";

            addToLists(_operator);
        }

        private void btnDivideAction()
        {
            string _operator = "÷";
            ExpressionString += $" {_operator} ";

            addToLists(_operator);
        }

        private void btnEqualsAction()
        {
            var format = new NumberFormatInfo();
            format.NegativeSign = "-";
            operands.Add(Int32.Parse(operand, format));
            operand = "0";
            calculate();
        }
        private void btnCEAction()
        {
            Result = 0;
            ExpressionString = "";
            operands.Clear();
            operators.Clear();
            operand = "";
        }
        #endregion

        #region Helper methods
        private void addToLists(string _operator)
        {
            if (operand[0] != '-')
            {
                operands.Add(Int32.Parse(operand));
            }
            else
            {
                var format = new NumberFormatInfo();
                format.NegativeSign = "-";
                operands.Add(Int32.Parse(operand, format));
            }

            operand = "";
            operators.Add(_operator);
        }

        void updateLists()
        {
            operators.RemoveAll(o => o == null);
            operands.RemoveAll(o => o == null);
        }
        #endregion

        #region Commands
        public Command Btn0 { get; }
        public Command Btn1 { get; }
        public Command Btn2 { get; }
        public Command Btn3 { get; }
        public Command Btn4 { get; }
        public Command Btn5 { get; }
        public Command Btn6 { get; }
        public Command Btn7 { get; }
        public Command Btn8 { get; }
        public Command Btn9 { get; }
        public Command BtnAdd { get; }
        public Command BtnSubtract { get; }
        public Command BtnMultiply { get; }
        public Command BtnDivide { get; }
        public Command BtnEquals { get; }
        public Command BtnCE { get; }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
