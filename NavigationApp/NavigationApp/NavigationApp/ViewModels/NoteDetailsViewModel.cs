﻿using NavigationApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NavigationApp.ViewModels
{
    public class NoteDetailsViewModel
    {
        private Note _note;

        public Note Note
        {
            get { return _note; }
            set { _note = value; }
        }

        public NoteDetailsViewModel(Note note)
        {
            Note = note;
        }
    }
}
