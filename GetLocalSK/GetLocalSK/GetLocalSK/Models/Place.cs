﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GetLocalSK.Models
{
    public class Place : Point
    {
        public byte[] HeaderImage { get; set; }

        public virtual List<Paragraph> Paragraphs { get; set; }
    }
}
